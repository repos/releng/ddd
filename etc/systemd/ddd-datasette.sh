#!/usr/bin/env bash

set -eo pipefail

if [ -z "$PORT" ]; then
    PORT=8001
fi
datasette --reload --metadata www/metadata.yaml -h 0.0.0.0 -p $PORT www
